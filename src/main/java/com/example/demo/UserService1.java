package com.example.demo;

import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService1 implements UserService {
    private final UserRepository repository;

    @Autowired
    public UserService1(UserRepository userRepository) {
        this.repository = userRepository;
    }


    @Override
    public User findById(Integer id) {
        return repository.getOne(id);
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }



    @Override
    public void saveUser(User user) {
        repository.save(user);
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }
}
