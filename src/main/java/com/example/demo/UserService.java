package com.example.demo;

import java.util.List;

public interface UserService {
    User findById(Integer id);
    List<User> findAll();
    void saveUser(User user);
    void deleteById(Integer id);
}

